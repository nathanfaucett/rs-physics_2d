extern crate gl;
extern crate gl_helpers;
extern crate glutin;

extern crate collide_2d;
extern crate imagefmt;
extern crate main_loop;
extern crate mat4;
extern crate physics_2d;
extern crate specs;
extern crate specs_bundler;
extern crate specs_camera;
extern crate specs_guided_join;
extern crate specs_sprite;
extern crate specs_time;
extern crate specs_transform;

use glutin::{Api, GlContext, GlRequest};
use gl::types::*;
use gl_helpers::*;

use imagefmt::{ColFmt, Image};
use specs_camera::{Camera2D, CameraBundle, ScreenSize};
use specs_sprite::{Sprite, SpriteBundle, SpriteGuide};
use specs_transform::{LocalTransform2D, Transform2D, TransformBundle};
use specs_time::{Time, TimeBundle};
use specs_guided_join::GuidedJoin;
use collide_2d::{Capsule, Circle, Convex, Line};
use physics_2d::{MotionState, Physics2DBundle, RigidBody2D, Shape2D};
use specs_bundler::SpecsBundler;
use specs::{DispatcherBuilder, Fetch, Join, ReadStorage, System, World};

static VERTEX_DATA: [GLfloat; 16] = [
    //   position     uv
     0.5f32,  0.5f32,   1f32, 1f32,
    -0.5f32,  0.5f32,   0f32, 1f32,
     0.5f32, -0.5f32,   1f32, 0f32,
    -0.5f32, -0.5f32,   0f32, 0f32
];

static VERTEX: &'static str = "
    uniform mat4 projection;
    uniform mat4 model_view;

    attribute vec2 position;
    attribute vec2 uv;

    varying vec2 v_uv;

    void main() {
        gl_Position = projection * model_view * vec4(position, 0, 1.0);
        v_uv = uv;
    }
";

static FRAGMENT: &'static str = "
    precision mediump float;

    uniform sampler2D diffuse;

    varying vec2 v_uv;

    void main() {
        gl_FragColor = texture2D(diffuse, v_uv);
    }
";

struct RenderSystem {
    view: [f32; 16],
    model: [f32; 16],
    model_view: [f32; 16],
    projection: [f32; 16],
    _image: Image<u8>,
    gl_texture: GLTexture,
    gl_program: GLProgram,
    _gl_buffer: GLBuffer,
    gl_vertex_array: GLVertexArray,
}

impl RenderSystem {
    pub fn new() -> Self {
        let gl_program = GLProgram::new(VERTEX, FRAGMENT);

        let gl_buffer = GLBuffer::new(BufferTarget::Array, 4, Usage::StaticDraw, &VERTEX_DATA);

        let mut gl_vertex_array = GLVertexArray::new();

        let image = imagefmt::read("examples/circle.png", ColFmt::RGBA).unwrap();

        let gl_texture = GLTexture::new_2d(
            image.w,
            image.h,
            InternalFormat::RGBA,
            DataFormat::RGBA,
            DataKind::UnsignedByte,
            FilterMode::Linear,
            Wrap::Repeat,
            true,
            &*image.buf,
        );

        gl_vertex_array.bind();
        gl_buffer.bind();
        gl_vertex_array.add_attribute(&gl_buffer, gl_program.get_attribute("position"), 0);
        gl_vertex_array.add_attribute(&gl_buffer, gl_program.get_attribute("uv"), 2);

        gl_set_clear_color(&[0.3, 0.3, 0.3, 1.0]);
        gl_clear(true, true, true);

        RenderSystem {
            view: mat4::new_identity(),
            model: mat4::new_identity(),
            model_view: mat4::new_identity(),
            projection: mat4::new_identity(),
            _image: image,
            gl_texture: gl_texture,
            gl_program: gl_program,
            _gl_buffer: gl_buffer,
            gl_vertex_array: gl_vertex_array,
        }
    }
}

impl<'a> System<'a> for RenderSystem {
    type SystemData = (
        ReadStorage<'a, Camera2D<f32>>,
        Fetch<'a, SpriteGuide>,
        ReadStorage<'a, Sprite<f32, usize>>,
        ReadStorage<'a, Transform2D<f32>>,
    );

    fn run(&mut self, (cameras, sprite_guide, sprites, transforms): Self::SystemData) {
        gl_clear(true, true, true);

        self.gl_program.bind();

        self.gl_vertex_array.bind();
        self.gl_vertex_array.enable_attributes();

        if let Some(camera) = (&cameras).join().nth(0) {
            mat4::from_mat32(&mut self.projection, camera.projection());
            mat4::from_mat32(&mut self.view, camera.view());

            for (_, transform) in (&sprites, &transforms).guided_join(sprite_guide.as_slice()) {
                mat4::from_mat32(&mut self.model, &transform.0);
                mat4::mul(&mut self.model_view, &self.model, &self.view);

                self.gl_program
                    .get_uniform("projection")
                    .set_mat4f(&self.projection);
                self.gl_program
                    .get_uniform("model_view")
                    .set_mat4f(&self.model_view);
                self.gl_program
                    .get_uniform("diffuse")
                    .set_sampler_2d(&self.gl_texture, 0);

                gl_draw_arrays(DrawMode::TriangleStrip, 0, 4);
            }
        }
    }
}

fn main() {
    let mut screen_width = 1024_usize;
    let mut screen_height = 768_usize;

    let mut events_loop = glutin::EventsLoop::new();
    let window = glutin::WindowBuilder::new()
        .with_title("Circle Collisions")
        .with_dimensions(screen_width as u32, screen_height as u32);
    let context = {
        let builder = glutin::ContextBuilder::new().with_vsync(true);

        if cfg!(target_os = "windows") {
            builder
        } else {
            builder.with_gl(GlRequest::Specific(Api::OpenGlEs, (2, 0)))
        }
    };
    let gl_window = glutin::GlWindow::new(window, context, &events_loop).unwrap();

    unsafe {
        gl_window.make_current().unwrap();
    }

    gl::load_with(|symbol| gl_window.get_proc_address(symbol) as *const _);

    let gl_info = GLInfo::new();
    println!("{}", gl_info.version());
    println!(
        "OpenGL version: {:?}.{:?}, GLSL version {:?}.{:?}0",
        gl_info.major(),
        gl_info.minor(),
        gl_info.glsl_major(),
        gl_info.glsl_minor()
    );

    gl_set_defaults();

    let mut world = World::new();

    let mut dispatcher = SpecsBundler::new(&mut world, DispatcherBuilder::new())
        .bundle(TimeBundle::<f32>::new())
        .unwrap()
        .bundle(SpriteBundle::<f32, usize>::new())
        .unwrap()
        .bundle(TransformBundle::<f32>::new())
        .unwrap()
        .bundle(CameraBundle::<f32>::new(screen_width, screen_height))
        .unwrap()
        .bundle(Physics2DBundle::<f32>::new())
        .unwrap()
        .add_thread_local(RenderSystem::new())
        .build();

    world
        .create_entity()
        .with(LocalTransform2D::<f32>::new())
        .with(Transform2D::<f32>::new())
        .with(Sprite::<f32, usize>::new(1))
        .with(
            RigidBody2D::<f32>::builder()
                .set_position(&[-3.0, 4.0])
                .set_motion_state(MotionState::Dynamic)
                .add_shape(Shape2D::<f32>::new([0.0, 0.0], 0.0, 1.0, Circle::new(0.5)))
                .build(),
        )
        .build();
    world
        .create_entity()
        .with(LocalTransform2D::<f32>::new())
        .with(Transform2D::<f32>::new())
        .with(Sprite::<f32, usize>::new(1))
        .with(
            RigidBody2D::<f32>::builder()
                .set_position(&[-3.0, -4.0])
                .add_shape(Shape2D::<f32>::new([0.0, 0.0], 0.0, 1.0, Circle::new(0.5)))
                .build(),
        )
        .build();

    world
        .create_entity()
        .with(LocalTransform2D::<f32>::new())
        .with(Transform2D::<f32>::new())
        .with(Sprite::<f32, usize>::new(1))
        .with(
            RigidBody2D::<f32>::builder()
                .set_position(&[-1.0, 4.0])
                .set_motion_state(MotionState::Dynamic)
                .add_shape(Shape2D::<f32>::new([0.0, 0.0], 0.0, 1.0, Circle::new(0.5)))
                .build(),
        )
        .build();
    world
        .create_entity()
        .with(LocalTransform2D::<f32>::new())
        .with(Transform2D::<f32>::new())
        .with(Sprite::<f32, usize>::new(1))
        .with(
            RigidBody2D::<f32>::builder()
                .set_position(&[-1.0, -4.0])
                .add_shape(Shape2D::<f32>::new([0.0, 0.5], 0.0, 1.0, Line::new(1.0)))
                .build(),
        )
        .build();

    world
        .create_entity()
        .with(LocalTransform2D::<f32>::new())
        .with(Transform2D::<f32>::new())
        .with(Sprite::<f32, usize>::new(1))
        .with(
            RigidBody2D::<f32>::builder()
                .set_position(&[1.0, 4.0])
                .set_motion_state(MotionState::Dynamic)
                .add_shape(Shape2D::<f32>::new([0.0, 0.0], 0.0, 1.0, Circle::new(0.5)))
                .build(),
        )
        .build();
    world
        .create_entity()
        .with(LocalTransform2D::<f32>::new())
        .with(Transform2D::<f32>::new())
        .with(Sprite::<f32, usize>::new(1))
        .with(
            RigidBody2D::<f32>::builder()
                .set_position(&[1.0, -4.0])
                .add_shape(Shape2D::<f32>::new(
                    [0.0, 0.0],
                    0.0,
                    1.0,
                    Capsule::new(0.5, 1.0),
                ))
                .build(),
        )
        .build();

    world
        .create_entity()
        .with(LocalTransform2D::<f32>::new())
        .with(Transform2D::<f32>::new())
        .with(Sprite::<f32, usize>::new(1))
        .with(
            RigidBody2D::<f32>::builder()
                .set_position(&[3.0, 4.0])
                .set_motion_state(MotionState::Dynamic)
                .add_shape(Shape2D::<f32>::new([0.0, 0.0], 0.0, 1.0, Circle::new(0.5)))
                .build(),
        )
        .build();
    world
        .create_entity()
        .with(LocalTransform2D::<f32>::new())
        .with(Transform2D::<f32>::new())
        .with(Sprite::<f32, usize>::new(1))
        .with(
            RigidBody2D::<f32>::builder()
                .set_position(&[3.0, -4.0])
                .add_shape(Shape2D::<f32>::new(
                    [0.0, 0.0],
                    0.0,
                    1.0,
                    Convex::new_box(1.0, 1.0),
                ))
                .build(),
        )
        .build();

    let mut camera = Camera2D::<f32>::new();
    camera.set_ortho_size(5.0);

    world
        .create_entity()
        .with(LocalTransform2D::<f32>::new())
        .with(Transform2D::<f32>::new())
        .with(camera)
        .build();

    main_loop::run_events_loop(&mut events_loop, move |events, _| {
        for event in events {
            match event {
                glutin::Event::WindowEvent { event, .. } => match event {
                    glutin::WindowEvent::Closed => {
                        let time = world.read_resource::<Time<f32>>();
                        println!("Total: {}s {}fps", time.total(), time.fps());
                        return main_loop::ControlFlow::Break;
                    }
                    glutin::WindowEvent::Resized(w, h) => {
                        screen_width = w as usize;
                        screen_height = h as usize;
                        gl_set_viewport(0, 0, screen_width, screen_height);

                        gl_window.resize(screen_width as u32, screen_height as u32);

                        let mut screen_size = world.write_resource::<ScreenSize>();
                        *screen_size = ScreenSize(screen_width, screen_height);
                    }
                    _ => (),
                },
                _ => (),
            }
        }

        dispatcher.dispatch(&mut world.res);
        gl_window.swap_buffers().unwrap();

        main_loop::ControlFlow::Continue
    });
}
