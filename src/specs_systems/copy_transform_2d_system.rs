use std::marker::PhantomData;

use type_name;
use number_traits::Float;
use specs::{Join, ReadStorage, System, WriteStorage};
use specs_transform::LocalTransform2D;

use vec2;

use super::super::RigidBody2D;

pub struct CopyTransform2DSystem<T>
where
    T: Copy + Float,
{
    _marker: PhantomData<T>,
}

impl<T> CopyTransform2DSystem<T>
where
    T: Copy + Float,
{
    #[inline(always)]
    pub fn new() -> Self {
        CopyTransform2DSystem {
            _marker: PhantomData,
        }
    }
}

impl<T> CopyTransform2DSystem<T>
where
    T: Copy + Float,
{
    #[inline]
    pub fn name() -> &'static str {
        type_name::get::<Self>()
    }
}

impl<'a, T> System<'a> for CopyTransform2DSystem<T>
where
    T: 'static + Send + Sync + Copy + Float,
{
    type SystemData = (
        WriteStorage<'a, LocalTransform2D<T>>,
        ReadStorage<'a, RigidBody2D<T>>,
    );

    fn run(&mut self, (mut locals, rigid_bodies): Self::SystemData) {
        for (local, rigid_body) in (&mut locals, &rigid_bodies).join() {
            vec2::copy(&mut local.position, rigid_body.position());
            local.rotation = rigid_body.rotation();
        }
    }
}
