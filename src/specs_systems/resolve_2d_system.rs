use std::marker::PhantomData;

use type_name;
use aabb2::AABB2;
use number_traits::Float;
use specs::{Entities, Entity, Fetch, Join, System, WriteStorage};
use collide_2d::{BoundingVolume2D, BroadPhase2D, BroadPhase2DAABB2, NearPhase2D,
                 NearPhase2DDefault};

use super::super::{MotionState, RigidBody2D, Solver2D, Solver2DGaussSeidel, Space2D};

pub struct Resolve2DSystem<
    T,
    BV = AABB2<T>,
    NP = NearPhase2DDefault<Entity, T, BV>,
    BP = BroadPhase2DAABB2<Entity, T>,
    S = Solver2DGaussSeidel<Entity, T>,
> where
    T: 'static + Copy + Float,
    BV: 'static + BoundingVolume2D<T>,
    NP: NearPhase2D<Entity, T, BV>,
    BP: BroadPhase2D<Entity, T, BV>,
    S: Solver2D<Entity, T>,
{
    _marker: PhantomData<(T, BV, NP, BP, S)>,
}

impl<T, BV, NP, BP, S> Resolve2DSystem<T, BV, NP, BP, S>
where
    T: 'static + Copy + Float,
    BV: 'static + BoundingVolume2D<T>,
    NP: NearPhase2D<Entity, T, BV>,
    BP: BroadPhase2D<Entity, T, BV>,
    S: Solver2D<Entity, T>,
{
    #[inline]
    pub fn name() -> &'static str {
        type_name::get::<Self>()
    }
}

impl<T, BV, NP, BP, S> Resolve2DSystem<T, BV, NP, BP, S>
where
    T: 'static + Copy + Float,
    BV: 'static + BoundingVolume2D<T>,
    NP: NearPhase2D<Entity, T, BV>,
    BP: BroadPhase2D<Entity, T, BV>,
    S: Solver2D<Entity, T>,
{
    #[inline(always)]
    pub fn new() -> Self {
        Resolve2DSystem {
            _marker: PhantomData,
        }
    }
}

impl<'a, T, BV, NP, BP, S> System<'a> for Resolve2DSystem<T, BV, NP, BP, S>
where
    T: 'static + Send + Sync + Copy + Float,
    BV: 'static + Send + Sync + BoundingVolume2D<T>,
    NP: 'static + Send + Sync + NearPhase2D<Entity, T, BV>,
    BP: 'static + Send + Sync + BroadPhase2D<Entity, T, BV>,
    S: 'static + Send + Sync + Solver2D<Entity, T>,
{
    type SystemData = (
        Fetch<'a, Space2D<Entity, T, BV, NP, BP, S>>,
        Entities<'a>,
        WriteStorage<'a, RigidBody2D<T, BV>>,
    );

    fn run(&mut self, (space, entities, mut rigid_bodies): Self::SystemData) {
        for (entity, rigid_body) in (&*entities, &mut rigid_bodies).join() {
            if let Some(vlambda) = space.vlambdas().get(&entity) {
                if rigid_body.motion_state() != &MotionState::Static {
                    rigid_body.add_velocity(&vlambda.vlambda);
                    rigid_body.add_angular_velocity(vlambda.wlambda);
                    rigid_body.add_position(&vlambda.xlambda);
                    rigid_body.add_rotation(vlambda.alambda);
                }
            }
        }
    }
}
