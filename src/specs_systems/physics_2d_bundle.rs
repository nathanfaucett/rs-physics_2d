use std::marker::PhantomData;

use specs::{DispatcherBuilder, Entity, World};
use specs_bundler::{Result, SpecsBundle};
use specs_time::TimeSystem;

use number_traits::Float;

use super::super::{RigidBody2D, Space2D};
use super::{CopyTransform2DSystem, Resolve2DSystem, Solver2DSystem};

#[derive(Default)]
pub struct Physics2DBundle<T> {
    _marker: PhantomData<T>,
}

impl<T> Physics2DBundle<T> {
    #[inline(always)]
    pub fn new() -> Self {
        Physics2DBundle {
            _marker: PhantomData,
        }
    }
}

impl<'a, 'b, T> SpecsBundle<'a, 'b> for Physics2DBundle<T>
where
    T: 'static + Sync + Send + Copy + Float,
{
    #[inline]
    fn build(
        self,
        world: &mut World,
        mut dispatch_builder: DispatcherBuilder<'a, 'b>,
    ) -> Result<DispatcherBuilder<'a, 'b>> {
        world.add_resource(Space2D::<Entity, T>::default());
        world.register::<RigidBody2D<T>>();

        dispatch_builder = dispatch_builder
            .add(
                Solver2DSystem::<f32>::new(),
                Solver2DSystem::<f32>::name(),
                &[TimeSystem::<f32>::name()],
            )
            .add(
                Resolve2DSystem::<f32>::new(),
                Resolve2DSystem::<f32>::name(),
                &[Solver2DSystem::<f32>::name()],
            )
            .add(
                CopyTransform2DSystem::<f32>::new(),
                CopyTransform2DSystem::<f32>::name(),
                &[Resolve2DSystem::<f32>::name()],
            );

        Ok(dispatch_builder)
    }
}
