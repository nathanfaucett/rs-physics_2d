#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum MotionState {
    Static,
    Dynamic,
    Kinematic,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum SleepState {
    Awake,
    Sleepy,
    Sleeping,
}
