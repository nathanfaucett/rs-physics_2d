use std::hash::Hash;
use std::marker::PhantomData;

use fnv::FnvHashMap;
use aabb2::AABB2;
use number_traits::Float;

use collide_2d::{BoundingVolume2D, BroadPhase2D, BroadPhase2DAABB2, NearPhase2D,
                 NearPhase2DDefault};

use super::{Lambda, RigidBody2D, Solver2D, Solver2DGaussSeidel};

pub struct Space2D<
    I,
    T,
    BV = AABB2<T>,
    NP = NearPhase2DDefault<I, T, BV>,
    BP = BroadPhase2DAABB2<I, T>,
    S = Solver2DGaussSeidel<I, T>,
> where
    I: Copy + Hash + Ord + Eq,
    T: 'static + Copy + Float,
    BV: 'static + BoundingVolume2D<T>,
    NP: NearPhase2D<I, T, BV>,
    BP: BroadPhase2D<I, T, BV>,
    S: Solver2D<I, T>,
{
    near_phase: NP,
    broad_phase: BP,
    solver: S,
    vlambdas: FnvHashMap<I, Lambda<T>>,
    gravity: [T; 2],
    _marker: PhantomData<(I, T, BV, NP, BP, S)>,
}

impl<I, T> Default for Space2D<I, T, AABB2<T>>
where
    I: Copy + Hash + Ord + Eq,
    T: 'static + Copy + Float,
{
    #[inline(always)]
    fn default() -> Self {
        Self::new(
            NearPhase2DDefault::default(),
            BroadPhase2DAABB2::default(),
            Solver2DGaussSeidel::default(),
        )
    }
}

impl<I, T, BV, NP, BP, S> Space2D<I, T, BV, NP, BP, S>
where
    I: Copy + Hash + Ord + Eq,
    T: 'static + Copy + Float,
    BV: 'static + BoundingVolume2D<T>,
    NP: NearPhase2D<I, T, BV>,
    BP: BroadPhase2D<I, T, BV>,
    S: Solver2D<I, T>,
{
    #[inline(always)]
    pub fn new(near_phase: NP, broad_phase: BP, solver: S) -> Self {
        Space2D {
            near_phase: near_phase,
            broad_phase: broad_phase,
            solver: solver,
            vlambdas: FnvHashMap::default(),
            gravity: [T::zero(), T::from_f32(-9.801)],
            _marker: PhantomData,
        }
    }
}

impl<I, T, BV, NP, BP, S> Space2D<I, T, BV, NP, BP, S>
where
    I: Copy + Hash + Ord + Eq,
    T: 'static + Copy + Float,
    BV: 'static + BoundingVolume2D<T>,
    NP: NearPhase2D<I, T, BV>,
    BP: BroadPhase2D<I, T, BV>,
    S: Solver2D<I, T>,
{
    #[inline(always)]
    pub fn gravity(&self) -> &[T; 2] {
        &self.gravity
    }
    #[inline(always)]
    pub fn gravity_mut(&mut self) -> &mut [T; 2] {
        &mut self.gravity
    }

    #[inline(always)]
    pub fn broad_phase(&self) -> &BP {
        &self.broad_phase
    }
    #[inline(always)]
    pub fn broad_phase_mut(&mut self) -> &mut BP {
        &mut self.broad_phase
    }

    #[inline(always)]
    pub fn near_phase(&self) -> &NP {
        &self.near_phase
    }
    #[inline(always)]
    pub fn near_phase_mut(&mut self) -> &mut NP {
        &mut self.near_phase
    }

    #[inline(always)]
    pub fn solver(&self) -> &S {
        &self.solver
    }
    #[inline(always)]
    pub fn solver_mut(&mut self) -> &mut S {
        &mut self.solver
    }

    #[inline(always)]
    pub fn set_vlambdas(&mut self, vlambdas: FnvHashMap<I, Lambda<T>>) {
        self.vlambdas = vlambdas;
    }
    #[inline(always)]
    pub fn vlambdas(&self) -> &FnvHashMap<I, Lambda<T>> {
        &self.vlambdas
    }

    #[inline]
    pub fn step<'a, It>(&mut self, iter: It, dt: T)
    where
        It: Iterator<Item = (I, &'a mut RigidBody2D<T, BV>)>,
    {
        let mut shapes = Vec::new();
        let mut vlambdas = FnvHashMap::default();

        for (entity, rigid_body) in iter {
            rigid_body.add_force(self.gravity());
            rigid_body.update(dt);

            for shape in rigid_body.shapes() {
                shapes.push((
                    entity,
                    &shape.position,
                    shape.rotation,
                    &shape.bounding_volume,
                    &shape.shape,
                ));
            }

            vlambdas.insert(
                entity,
                Lambda::new(
                    rigid_body.inv_mass(),
                    rigid_body.inv_inertia(),
                    *rigid_body.motion_state(),
                    rigid_body.velocity().clone(),
                    rigid_body.angular_velocity(),
                ),
            );
        }

        let pairs = self.broad_phase_mut().run(&*shapes);
        let contacts = self.near_phase_mut().run(&*shapes, &*pairs);

        self.solver_mut().solve(&*contacts, &mut vlambdas);
        self.set_vlambdas(vlambdas);
    }
}
