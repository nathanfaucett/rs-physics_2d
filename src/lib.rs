extern crate aabb2;
extern crate collide_2d;
extern crate fnv;
extern crate number_traits;
#[cfg(feature = "use_specs")]
extern crate specs;
#[cfg(feature = "specs")]
extern crate specs_bundler;
#[cfg(feature = "use_specs")]
extern crate specs_time;
#[cfg(feature = "use_specs")]
extern crate specs_transform;
extern crate type_name;
extern crate vec2;

mod body_2d;
mod solver_2d;
#[cfg(feature = "use_specs")]
mod specs_systems;
mod enums;
mod space_2d;

pub use self::body_2d::*;
pub use self::solver_2d::*;
#[cfg(feature = "use_specs")]
pub use self::specs_systems::*;
pub use self::enums::{MotionState, SleepState};
pub use self::space_2d::Space2D;
