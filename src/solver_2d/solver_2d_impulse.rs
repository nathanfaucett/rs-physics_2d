use std::hash::Hash;
use std::marker::PhantomData;

use fnv::FnvHashMap;
use vec2;
use number_traits::Float;
use collide_2d::Contact2D;

use super::super::{Lambda, MotionState};
use super::Solver2D;

pub struct Solver2DImpulse<I, T>
where
    I: Copy + Hash + Ord + Eq,
    T: Copy + Float,
{
    _marker: PhantomData<(I, T)>,
}

impl<I, T> Default for Solver2DImpulse<I, T>
where
    I: Copy + Hash + Ord + Eq,
    T: Copy + Float,
{
    #[inline(always)]
    fn default() -> Self {
        Solver2DImpulse {
            _marker: PhantomData,
        }
    }
}

impl<I, T> Solver2DImpulse<I, T>
where
    I: Copy + Hash + Ord + Eq,
    T: Copy + Float,
{
    #[inline(always)]
    pub fn new() -> Self {
        Self::default()
    }
}

impl<I, T> Solver2D<I, T> for Solver2DImpulse<I, T>
where
    I: Copy + Hash + Ord + Eq,
    T: Copy + Float,
{
    fn solve(&mut self, contacts: &[Contact2D<I, T>], vlambdas: &mut FnvHashMap<I, Lambda<T>>) {
        let mut v = [T::zero(); 2];
        let mut i = [T::zero(); 2];
        let mut t = [T::zero(); 2];
        let mut c = [T::zero(); 2];

        for contact in contacts {
            let inv_mass;
            {
                let lambda_i = vlambdas.get(&contact.id_i()).unwrap();
                let lambda_j = vlambdas.get(&contact.id_j()).unwrap();

                vec2::sub(&mut v, lambda_j.velocity(), lambda_i.velocity());
                let vn = vec2::dot(&v, contact.normal());

                inv_mass = lambda_i.inv_mass() + lambda_j.inv_mass();

                if vn < T::zero() {
                    let e = T::from_f32(0.95);
                    let j = (-(T::one() + e) * vn) / inv_mass;
                    vec2::smul(&mut i, contact.normal(), j);
                }
            }

            let percent = T::from_f32(0.2);
            let slop = T::from_f32(0.01);
            let depth = (contact.depth() - slop).max(&T::zero());
            vec2::smul(&mut c, contact.normal(), depth / (inv_mass * percent));

            {
                let lambda_i = vlambdas.get_mut(&contact.id_i()).unwrap();

                if lambda_i.motion_state() != &MotionState::Static {
                    vec2::smul(&mut t, &i, lambda_i.inv_mass());

                    lambda_i.vlambda[0] -= t[0];
                    lambda_i.vlambda[1] -= t[1];

                    lambda_i.xlambda[0] -= c[0];
                    lambda_i.xlambda[1] -= c[1];
                }
            }
            {
                let lambda_j = vlambdas.get_mut(&contact.id_j()).unwrap();

                if lambda_j.motion_state() != &MotionState::Static {
                    vec2::smul(&mut t, &i, lambda_j.inv_mass());

                    lambda_j.vlambda[0] += t[0];
                    lambda_j.vlambda[1] += t[1];

                    lambda_j.xlambda[0] += c[0];
                    lambda_j.xlambda[1] += c[1];
                }
            }
        }
    }
}
