use std::hash::Hash;

use fnv::FnvHashMap;
use number_traits::Float;
use collide_2d::Contact2D;

use super::super::Lambda;

pub trait Solver2D<I, T>: Default
where
    I: Copy + Hash + Ord + Eq,
    T: Copy + Float,
{
    fn solve(&mut self, &[Contact2D<I, T>], &mut FnvHashMap<I, Lambda<T>>);
}
