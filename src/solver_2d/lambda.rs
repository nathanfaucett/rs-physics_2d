use number_traits::Float;

use super::super::MotionState;

#[derive(Debug, Clone)]
pub struct Lambda<T>
where
    T: Copy + Float,
{
    inv_mass: T,
    inv_inertia: T,
    motion_state: MotionState,
    velocity: [T; 2],
    angular_velocity: T,
    pub vlambda: [T; 2],
    pub wlambda: T,
    pub xlambda: [T; 2],
    pub alambda: T,
}

impl<T> Lambda<T>
where
    T: Copy + Float,
{
    #[inline(always)]
    pub fn new(
        inv_mass: T,
        inv_inertia: T,
        motion_state: MotionState,
        velocity: [T; 2],
        angular_velocity: T,
    ) -> Self {
        Lambda {
            inv_mass: inv_mass,
            inv_inertia: inv_inertia,
            motion_state: motion_state,
            velocity: velocity,
            angular_velocity: angular_velocity,
            vlambda: [T::zero(); 2],
            wlambda: T::zero(),
            xlambda: [T::zero(); 2],
            alambda: T::zero(),
        }
    }

    #[inline(always)]
    pub fn inv_mass(&self) -> T {
        self.inv_mass
    }
    #[inline(always)]
    pub fn inv_inertia(&self) -> T {
        self.inv_inertia
    }
    #[inline(always)]
    pub fn motion_state(&self) -> &MotionState {
        &self.motion_state
    }

    #[inline(always)]
    pub fn velocity(&self) -> &[T; 2] {
        &self.velocity
    }
    #[inline(always)]
    pub fn angular_velocity(&self) -> T {
        self.angular_velocity
    }
}
