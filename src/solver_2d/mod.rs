mod lambda;
mod solver_2d_gauss_seidel;
mod solver_2d_impulse;
mod solver_2d;

pub use self::lambda::Lambda;
pub use self::solver_2d_gauss_seidel::Solver2DGaussSeidel;
pub use self::solver_2d_impulse::Solver2DImpulse;
pub use self::solver_2d::Solver2D;
