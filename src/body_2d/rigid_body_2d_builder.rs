use std::f64::consts::PI;

use aabb2::AABB2;
use number_traits::Float;
use vec2;
use collide_2d::BoundingVolume2D;

use super::super::MotionState;
use super::{RigidBody2D, Shape2D};

pub struct RigidBody2DBuilder<T, B = AABB2<T>>
where
    T: 'static + Copy + Float,
    B: 'static + BoundingVolume2D<T>,
{
    shapes: Vec<Shape2D<T, B>>,

    position: [T; 2],
    velocity: [T; 2],
    force: [T; 2],
    linear_damping: T,

    rotation: T,
    angular_velocity: T,
    torque: T,
    angular_damping: T,

    motion_state: MotionState,
    allow_sleep: bool,
}

impl<T, B> RigidBody2DBuilder<T, B>
where
    T: 'static + Copy + Float,
    B: 'static + BoundingVolume2D<T>,
{
    #[inline(always)]
    pub fn new() -> Self {
        RigidBody2DBuilder {
            shapes: Vec::new(),

            position: [T::zero(); 2],
            velocity: [T::zero(); 2],
            force: [T::zero(); 2],
            linear_damping: T::from_f32(0.01),

            rotation: T::zero(),
            angular_velocity: T::zero(),
            torque: T::zero(),
            angular_damping: T::from_f64(PI * 0.02),

            motion_state: MotionState::Static,
            allow_sleep: true,
        }
    }

    #[inline(always)]
    pub fn build(self) -> RigidBody2D<T, B> {
        RigidBody2D::new(
            self.shapes,
            self.position,
            self.velocity,
            self.force,
            self.linear_damping,
            self.rotation,
            self.angular_velocity,
            self.torque,
            self.angular_damping,
            self.motion_state,
            self.allow_sleep,
        )
    }

    #[inline(always)]
    pub fn add_shape(mut self, shape: Shape2D<T, B>) -> Self {
        self.shapes.push(shape);
        self
    }

    #[inline(always)]
    pub fn set_motion_state(mut self, motion_state: MotionState) -> Self {
        self.motion_state = motion_state;
        self
    }
    #[inline(always)]
    pub fn set_allow_sleep(mut self, allow_sleep: bool) -> Self {
        self.allow_sleep = allow_sleep;
        self
    }

    #[inline(always)]
    pub fn set_position(mut self, position: &[T; 2]) -> Self {
        vec2::copy(&mut self.position, position);
        self
    }
    #[inline(always)]
    pub fn set_velocity(mut self, velocity: &[T; 2]) -> Self {
        vec2::copy(&mut self.velocity, velocity);
        self
    }
    #[inline(always)]
    pub fn set_force(mut self, force: &[T; 2]) -> Self {
        vec2::copy(&mut self.force, force);
        self
    }
    #[inline(always)]
    pub fn set_linear_damping(mut self, linear_damping: T) -> Self {
        self.linear_damping = linear_damping;
        self
    }

    #[inline(always)]
    pub fn set_rotation(mut self, rotation: T) -> Self {
        self.rotation = rotation;
        self
    }
    #[inline(always)]
    pub fn set_angular_velocity(mut self, angular_velocity: T) -> Self {
        self.angular_velocity = angular_velocity;
        self
    }
    #[inline(always)]
    pub fn set_torque(mut self, torque: T) -> Self {
        self.torque = torque;
        self
    }
    #[inline(always)]
    pub fn set_angular_damping(mut self, angular_damping: T) -> Self {
        self.angular_damping = angular_damping;
        self
    }
}
