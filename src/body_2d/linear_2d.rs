use number_traits::Float;

use super::super::SleepState;

#[derive(Debug, Clone, PartialEq, PartialOrd)]
pub struct Linear2D<T>
where
    T: Copy + Float,
{
    pub position: [T; 2],
    pub velocity: [T; 2],
    pub force: [T; 2],
    pub damping: T,
}

impl<T> Default for Linear2D<T>
where
    T: Copy + Float,
{
    #[inline(always)]
    fn default() -> Self {
        Self::new(
            [T::zero(); 2],
            [T::zero(); 2],
            [T::zero(); 2],
            T::from_f32(0.01),
        )
    }
}

impl<T> Linear2D<T>
where
    T: Copy + Float,
{
    #[inline(always)]
    pub fn new(position: [T; 2], velocity: [T; 2], force: [T; 2], damping: T) -> Self {
        Linear2D {
            position: position,
            velocity: velocity,
            force: force,
            damping: damping,
        }
    }

    #[inline]
    pub fn update(&mut self, dt: T, inv_mass: T, sleep_state: &SleepState) -> &mut Self {
        let damping = (T::one() - self.damping).powf(&dt);

        self.velocity[0] += self.force[0] * inv_mass * dt;
        self.velocity[1] += self.force[1] * inv_mass * dt;
        self.force[0] = T::zero();
        self.force[1] = T::zero();

        self.velocity[0] *= damping;
        self.velocity[1] *= damping;

        if sleep_state != &SleepState::Sleeping {
            self.position[0] += self.velocity[0] * dt;
            self.position[1] += self.velocity[1] * dt;
        }

        self
    }
}
