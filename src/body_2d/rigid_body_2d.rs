use aabb2::AABB2;
use number_traits::Float;
use vec2;
#[cfg(feature = "use_specs")]
use specs::{Component, VecStorage};
use collide_2d::BoundingVolume2D;

use super::super::{MotionState, SleepState};
use super::{Angular2D, Linear2D, RigidBody2DBuilder, Shape2D};

pub struct RigidBody2D<T, B = AABB2<T>>
where
    T: 'static + Copy + Float,
    B: 'static + BoundingVolume2D<T>,
{
    linear: Linear2D<T>,
    angular: Angular2D<T>,

    shapes: Vec<Shape2D<T, B>>,

    mass: T,
    inv_mass: T,
    inertia: T,
    inv_inertia: T,

    motion_state: MotionState,

    allow_sleep: bool,
    sleep_state: SleepState,

    sleep_time: T,
    last_sleepy_time: T,
}

unsafe impl<T, B> Send for RigidBody2D<T, B>
where
    T: 'static + Send + Copy + Float,
    B: 'static + Send + BoundingVolume2D<T>,
{
}
unsafe impl<T, B> Sync for RigidBody2D<T, B>
where
    T: 'static + Sync + Copy + Float,
    B: 'static + Sync + BoundingVolume2D<T>,
{
}

#[cfg(feature = "use_specs")]
impl<T, B> Component for RigidBody2D<T, B>
where
    T: 'static + Send + Sync + Copy + Float,
    B: 'static + Send + Sync + BoundingVolume2D<T>,
{
    type Storage = VecStorage<Self>;
}

impl<T, B> Default for RigidBody2D<T, B>
where
    T: 'static + Copy + Float,
    B: 'static + BoundingVolume2D<T>,
{
    #[inline(always)]
    fn default() -> Self {
        RigidBody2D {
            linear: Linear2D::default(),
            angular: Angular2D::default(),

            shapes: Vec::new(),

            mass: T::zero(),
            inv_mass: T::zero(),
            inertia: T::zero(),
            inv_inertia: T::zero(),

            motion_state: MotionState::Static,

            allow_sleep: true,
            sleep_state: SleepState::Awake,

            sleep_time: T::zero(),
            last_sleepy_time: T::zero(),
        }
    }
}

impl<T, B> RigidBody2D<T, B>
where
    T: Copy + Float,
    B: 'static + BoundingVolume2D<T>,
{
    #[inline(always)]
    pub fn builder() -> RigidBody2DBuilder<T> {
        RigidBody2DBuilder::new()
    }

    #[inline(always)]
    pub fn new(
        shapes: Vec<Shape2D<T, B>>,

        position: [T; 2],
        velocity: [T; 2],
        force: [T; 2],
        linear_damping: T,

        rotation: T,
        angular_velocity: T,
        torque: T,
        angular_damping: T,

        motion_state: MotionState,
        allow_sleep: bool,
    ) -> Self {
        let mut rigid_body = RigidBody2D {
            linear: Linear2D::new(position, velocity, force, linear_damping),
            angular: Angular2D::new(rotation, angular_velocity, torque, angular_damping),

            shapes: shapes,

            mass: T::max_value(),
            inv_mass: T::zero(),
            inertia: T::max_value(),
            inv_inertia: T::zero(),

            motion_state: motion_state,

            allow_sleep: allow_sleep,
            sleep_state: SleepState::Awake,

            sleep_time: T::zero(),
            last_sleepy_time: T::zero(),
        };

        rigid_body.reset_mass_data();
        rigid_body.update_shapes();

        rigid_body
    }

    #[inline]
    pub fn add_force(&mut self, force: &[T; 2]) {
        let current = self.linear.force.clone();
        vec2::add(&mut self.linear.force, &current, force);
    }
    #[inline]
    pub fn sub_force(&mut self, force: &[T; 2]) {
        let current = self.linear.force.clone();
        vec2::sub(&mut self.linear.force, &current, force);
    }

    #[inline]
    pub fn add_velocity(&mut self, velocity: &[T; 2]) {
        let current = self.linear.velocity.clone();
        vec2::add(&mut self.linear.velocity, &current, velocity);
    }
    #[inline]
    pub fn sub_velocity(&mut self, velocity: &[T; 2]) {
        let current = self.linear.velocity.clone();
        vec2::sub(&mut self.linear.velocity, &current, velocity);
    }
    #[inline(always)]
    pub fn add_angular_velocity(&mut self, angular_velocity: T) {
        self.angular.velocity += angular_velocity;
    }
    #[inline(always)]
    pub fn sub_angular_velocity(&mut self, angular_velocity: T) {
        self.angular.velocity -= angular_velocity;
    }

    #[inline]
    pub fn add_position(&mut self, position: &[T; 2]) {
        let current = self.linear.position.clone();
        vec2::add(&mut self.linear.position, &current, position);
    }
    #[inline]
    pub fn sub_position(&mut self, position: &[T; 2]) {
        let current = self.linear.position.clone();
        vec2::sub(&mut self.linear.position, &current, position);
    }
    #[inline(always)]
    pub fn add_rotation(&mut self, rotation: T) {
        self.angular.rotation += rotation;
    }
    #[inline(always)]
    pub fn sub_rotation(&mut self, rotation: T) {
        self.angular.rotation -= rotation;
    }

    #[inline(always)]
    pub fn reset_mass_data(&mut self) {
        let mut total_mass = T::zero();
        let mut total_inertia = T::zero();

        for shape in self.shapes.iter() {
            let mass = shape.shape.area() * shape.density;
            let offset = vec2::dot(&shape.position, &shape.position);
            let inertia = mass * shape.shape.inertia() + offset;
            total_mass += mass;
            total_inertia += inertia;
        }

        self.set_mass(total_mass);
        self.set_inertia(total_inertia);
    }

    #[inline]
    pub fn set_mass(&mut self, mass: T) {
        self.mass = mass;
        self.inv_mass = T::one() / mass;
    }

    #[inline]
    pub fn set_inertia(&mut self, inertia: T) {
        self.inertia = inertia;
        self.inv_inertia = T::one() / inertia;
    }

    #[inline]
    pub fn update(&mut self, dt: T) -> &mut Self {
        if &self.motion_state != &MotionState::Static {
            self.linear.update(dt, self.inv_mass, &self.sleep_state);
            self.angular.update(dt, self.inv_inertia, &self.sleep_state);
            self.update_shapes();
        }
        self
    }

    #[inline]
    fn update_shapes(&mut self) {
        let position = self.position().clone();
        let rotation = self.rotation();

        for shape in self.shapes.iter_mut() {
            shape.update(&position, rotation);
        }
    }

    #[inline(always)]
    pub fn mass(&self) -> T {
        self.mass
    }
    #[inline(always)]
    pub fn inv_mass(&self) -> T {
        self.inv_mass
    }

    #[inline(always)]
    pub fn inertia(&self) -> T {
        self.inertia
    }
    #[inline(always)]
    pub fn inv_inertia(&self) -> T {
        self.inv_inertia
    }

    #[inline(always)]
    pub fn motion_state(&self) -> &MotionState {
        &self.motion_state
    }
    #[inline(always)]
    pub fn shapes(&self) -> &[Shape2D<T, B>] {
        &*self.shapes
    }

    #[inline(always)]
    pub fn position(&self) -> &[T; 2] {
        &self.linear.position
    }
    #[inline(always)]
    pub fn velocity(&self) -> &[T; 2] {
        &self.linear.velocity
    }
    #[inline(always)]
    pub fn force(&self) -> &[T; 2] {
        &self.linear.force
    }
    #[inline(always)]
    pub fn linear_damping(&self) -> T {
        self.linear.damping
    }

    #[inline(always)]
    pub fn rotation(&self) -> T {
        self.angular.rotation
    }
    #[inline(always)]
    pub fn angular_velocity(&self) -> T {
        self.angular.velocity
    }
    #[inline(always)]
    pub fn torque(&self) -> T {
        self.angular.torque
    }
    #[inline(always)]
    pub fn angular_damping(&self) -> T {
        self.angular.damping
    }

    #[inline(always)]
    pub fn position_mut(&mut self) -> &mut [T; 2] {
        &mut self.linear.position
    }
    #[inline(always)]
    pub fn velocity_mut(&mut self) -> &mut [T; 2] {
        &mut self.linear.velocity
    }
    #[inline(always)]
    pub fn force_mut(&mut self) -> &mut [T; 2] {
        &mut self.linear.force
    }

    #[inline(always)]
    pub fn rotation_mut(&mut self) -> &mut T {
        &mut self.angular.rotation
    }
    #[inline(always)]
    pub fn angular_velocity_mut(&mut self) -> &mut T {
        &mut self.angular.velocity
    }
    #[inline(always)]
    pub fn torque_mut(&mut self) -> &mut T {
        &mut self.angular.torque
    }
}
