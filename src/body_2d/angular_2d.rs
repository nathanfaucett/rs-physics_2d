use number_traits::Float;

use super::super::SleepState;

#[derive(Debug, Clone, PartialEq, PartialOrd)]
pub struct Angular2D<T>
where
    T: Copy + Float,
{
    pub rotation: T,
    pub velocity: T,
    pub torque: T,
    pub damping: T,
}

impl<T> Default for Angular2D<T>
where
    T: Copy + Float,
{
    #[inline(always)]
    fn default() -> Self {
        Self::new(T::zero(), T::zero(), T::zero(), T::PI() * T::from_f32(0.02))
    }
}

impl<T> Angular2D<T>
where
    T: Copy + Float,
{
    #[inline(always)]
    pub fn new(rotation: T, velocity: T, torque: T, damping: T) -> Self {
        Angular2D {
            rotation: rotation,
            velocity: velocity,
            torque: torque,
            damping: damping,
        }
    }

    #[inline]
    pub fn update(&mut self, dt: T, inv_inertia: T, sleep_state: &SleepState) -> &mut Self {
        let damping = (T::one() - self.damping).powf(&dt);

        self.velocity += self.torque * inv_inertia * dt;
        self.torque = T::zero();
        self.velocity *= damping;

        if sleep_state != &SleepState::Sleeping {
            self.rotation += self.velocity * dt;
        }

        self
    }
}
