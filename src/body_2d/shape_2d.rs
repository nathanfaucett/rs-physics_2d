use aabb2::AABB2;
use vec2;
use number_traits::Float;
use collide_2d::{self, BoundingVolume2D};

pub struct Shape2D<T, B = AABB2<T>>
where
    T: 'static + Copy + Float,
    B: 'static + BoundingVolume2D<T>,
{
    pub local_position: [T; 2],
    pub local_rotation: T,

    pub position: [T; 2],
    pub rotation: T,

    pub density: T,
    pub bounding_volume: B,
    pub shape: Box<collide_2d::Shape2D<T, B>>,
}

impl<T, B> Shape2D<T, B>
where
    T: 'static + Copy + Float,
    B: 'static + BoundingVolume2D<T>,
{
    #[inline(always)]
    pub fn new<S>(local_position: [T; 2], local_rotation: T, density: T, shape: S) -> Self
    where
        S: 'static + collide_2d::Shape2D<T, B>,
    {
        Shape2D {
            local_position: local_position,
            local_rotation: local_rotation,
            position: [T::zero(); 2],
            rotation: T::zero(),
            density: density,
            bounding_volume: B::default(),
            shape: Box::new(shape),
        }
    }
}

impl<T, B> Shape2D<T, B>
where
    T: 'static + Copy + Float,
    B: 'static + BoundingVolume2D<T>,
{
    #[inline]
    pub fn update(&mut self, position: &[T; 2], rotation: T) {
        vec2::add(&mut self.position, position, &self.local_position);
        self.rotation = rotation + self.local_rotation;
        self.shape
            .bounding_volume(&mut self.bounding_volume, &self.position, self.rotation);
    }
}
